package com.maxima.library.service;

import com.maxima.library.entity.Book;
import com.maxima.library.entity.Customer;
import com.maxima.library.entity.Rent;
import com.maxima.library.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class BookServiceImpl implements BooksService {
    @Autowired
    private EntityManager entityManager;

    private final BookRepository bookRepository;

    @Override
    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    @Transactional
    @Override
    public Book saveBook(Book book) {;
        return bookRepository.save(book);
    }

    @Override
    public Book getBook(Long bookId) {
        return entityManager.find(Book.class, bookId);
    }

    @Override
    public void deleteBook(Long bookId) {
        bookRepository.deleteById(bookId);
    }

    @Override
    public List<Book> findBookByTitleOrDescription(String description) {
        List<Book> bookList = bookRepository.findAll();
        String descriptionToLower = description.toLowerCase();

        return bookList.stream()
                .filter(book ->
                        book.getTitle().toLowerCase().contains(descriptionToLower)
                                || book.getDescription().toLowerCase().contains(descriptionToLower)
                )
                .collect(Collectors.toList());
    }
}