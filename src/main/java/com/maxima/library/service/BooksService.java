package com.maxima.library.service;

import com.maxima.library.entity.Book;


import java.util.List;


public interface BooksService {
    List<Book> getAllBooks();

    Book saveBook(Book employee);

    Book getBook(Long employeeId);

    void deleteBook(Long employeeId);

    List<Book> findBookByTitleOrDescription (String description);
}
