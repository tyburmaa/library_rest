package com.maxima.library.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class UnavailableBook extends RuntimeException {
    public UnavailableBook(String message) {
        super(message);
    }
}
