package com.maxima.library.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class ReceivedBook extends RuntimeException {
    public ReceivedBook(String message) {
        super(message);
    }
}
