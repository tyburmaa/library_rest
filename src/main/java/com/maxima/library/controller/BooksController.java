package com.maxima.library.controller;

import com.maxima.library.entity.Book;
import com.maxima.library.entity.Rent;
import com.maxima.library.exception.NoSuchBookException;
import com.maxima.library.exception.ReceivedBook;
import com.maxima.library.exception.UnavailableBook;
import com.maxima.library.service.BooksService;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class BooksController {
    private final BooksService booksService;

    @GetMapping("/books")
    public List<Book> getAllBooks() {
        return booksService.getAllBooks();
    }

    @GetMapping("/books/{id}")
    public Book getBook(@PathVariable long id) {
        return booksService.getBook(id);
    }

    @PostMapping("/books")
    public ResponseEntity<Book> addNewBook(@RequestBody Book book) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(booksService.saveBook(book));
    }

    @PutMapping("/books/{id}")
    public Book updateBook(@RequestBody Book book) {
        booksService.saveBook(book);
        return book;
    }

    @DeleteMapping("/books/{id}")
    public String deleteBook(@PathVariable long id) {
        Book book = booksService.getBook(id);
        if (book == null) {
            throw new NoSuchBookException("There is no book with ID = " + id + " in Database");
        }
        booksService.deleteBook(id);
        return "Book with ID = " + id + " was deleted";
    }

    @PostMapping("books/{description}")
    public List<Book> findBook(@PathVariable String description) {
        return booksService.findBookByTitleOrDescription(description);
    }

    @PostMapping("/rent")
    public ResponseEntity<Book> lendBook(@RequestBody Rent rent) {
        Book book = booksService.getBook(rent.getBookId());

        if (book == null) {
            throw new NoSuchBookException("There is no book with ID = " + rent.getBookId() + " in Database");
        }

        if (book.getAmount() == 0) {
            throw new UnavailableBook("No books available");
        }

        List<Rent> rentList = book.getRentList();

        if (rentList.stream()
                .anyMatch(
                        bookRent -> bookRent.getCustomerId().equals(rent.getCustomerId()) && bookRent.getActive()
                )
        ) {
            throw new ReceivedBook("The book has already been received by this person");
        }

        rent.setActive(true);
        rent.setTransferDate(new Date());

        rentList.add(rent);

        book.setRentList(rentList);
        book.setAmount(book.getAmount() - 1);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(booksService.saveBook(book));
    }

    @PutMapping("/rent")
    public Book returnBook(@RequestBody Rent rent) {
        Book book = booksService.getBook(rent.getBookId());

        if (book == null) {
            throw new NoSuchBookException("There is no book with ID = " + rent.getBookId() + " in Database");
        }

        List<Rent> rentsOfCustomer = book.getRentList().stream()
                .filter(bookRent -> bookRent.getCustomerId().equals(rent.getCustomerId()))
                .toList();

        if (rentsOfCustomer.stream().noneMatch(Rent::getActive)) {
            throw new RuntimeException("The book hadn't been given to this person");
        }

        Rent activeRent = rentsOfCustomer.stream().filter(Rent::getActive).findAny().orElse(new Rent());
        activeRent.setActive(false);
        activeRent.setReceiptDate(new Date());

        book.setAmount(book.getAmount() + 1);

        booksService.saveBook(book);

        return book;
    }
}
