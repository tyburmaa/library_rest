package com.maxima.library.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "books")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "title")
    @Size(min = 3, message = "Size should contain at least 3 symbols")
    private String title;
    @Column(name = "description")
    @Size(min = 10, max = 50, message = "Please, make the description longer")
    private String description;
    @Column(name = "amount")
    private Integer amount;
    @OneToMany(mappedBy = "bookId", cascade = CascadeType.ALL)
    private List<Rent> rentList;
  //  @JoinTable(name = "book_rental", joinColumns = @JoinColumn(name = "book_id"))
}
