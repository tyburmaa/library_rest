package com.maxima.library.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "book_rental")
public class Rent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "book_id")
    private Long bookId;

    @Column(name = "customer_id")
    private Long customerId;

    @Column(name = "transfer_date")
    private Date transferDate;

    @Column(name = "receipt_date")
    private Date receiptDate;

    @Column(name = "active")
    private Boolean active;

}
